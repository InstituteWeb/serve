<?php

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$boot = function ($extensionKey) {
    if (TYPO3_MODE === 'BE') {
        // AfterSave hook
        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass']['serve'] =
            'InstituteWeb\\Serve\\Hooks\\AfterSaveHook';

        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['typo3/class.db_list_extra.inc']['actions']['serve_dumpButton'] =
            'InstituteWeb\\Serve\\Hooks\\DumpButton';
    }
};

$boot($_EXTKEY);
unset($boot);
