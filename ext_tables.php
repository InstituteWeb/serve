<?php

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$boot = function ($extensionKey) {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'InstituteWeb.' . $extensionKey,
        'system',
        'serveModule',
        '',
        [
            'Rule' => 'index,show',
            'Serve' => 'saveRecord,importRecord'
        ],
        [
            'access' => 'none',
//            'icon' => $extensionIconPath,
            'labels' => 'LLL:EXT:' . $extensionKey . '/Resources/Private/Language/locallang_mod.xml',
        ]
    );

    // Register PageTS defaults
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('tx_serve {
        defaults {
            dataDirectory = fileadmin/data/
        }
        
        # SysRegistry // Database // File
        mapper.default.driver = SysRegistry
        
        rules {
            sys_template {
                enabled = 1
                mapper = default
                
                processor = FileProcessor
                processor {
                    format = json
                    directory < tx_serve.defaults.dataDirectory
                    fileName = {identifier}.json
                
                    allow = uid,pid,title,sitetitle,root
                    #exclude = tstamp,crdate
                }
                
                processors {
                    10 = FileProcessor
                    10 {
                        format = json
                        directory < tx_serve.defaults.dataDirectory
                        fileName = {identifier}.json
                    
                        allow = uid,pid,title,sitetitle,root
                        #exclude = tstamp,crdate
                    }
                }
                
            }
        
        
            tx_dce_domain_model_dce {
                enabled = 0
                
                processors {
                    10 = PackageProcessor
                    10 {
                        file = 
                        ignore = tstamp,crdate
                        outsourceFields {
                            template_content {
                                file = template.html
                            }
                        }
                        includeChildrecords = tx_domain_model_dcefield
                        includeChildrecords {
                            tx_domain_model_dcefield {
                                
                            }
                        }
                    }
                }
                
            }
        }
    }');

// How to register icons:
//    /** @var \TYPO3\CMS\Core\Imaging\IconRegistry $iconRegistry */
//    $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Core\Imaging\IconRegistry');
//    $iconRegistry->registerIcon(
//        'icon-identifier',
//        'TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider',
//        ['source' => 'EXT:serve/Resources/Public/Icons/icon.png']
//    );
};

$boot($_EXTKEY);
unset($boot);
