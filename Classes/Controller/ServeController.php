<?php
namespace InstituteWeb\Serve\Controller;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Serve\Domain\ValueObject\SystemRecordIdentifier;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class ServeController
 *
 * @package InstituteWeb\Serve
 */
class ServeController extends AbstractBackendController
{
    /**
     * @var \InstituteWeb\Serve\Domain\Repository\RuleRepository
     * @inject
     */
    protected $ruleRepository;

    /**
     * @var \InstituteWeb\Serve\Domain\Repository\RecordRepository
     * @inject
     */
    protected $recordRepository;

    /**
     * @param string $identifier
     * @param string $table
     * @param int $uid
     * @TODO Use extbase parameters
     * @TODO And add field list
     */
    public function saveRecordAction($identifier, $table, $uid)
    {
        $mapping = null;
        if ($this->ruleRepository->getDefaultMappingContainer()->has($identifier)) {
            $mapping = $this->ruleRepository->getDefaultMappingContainer()->get($identifier);
        }

        $systemRecordIdentifier = new SystemRecordIdentifier($table . ':' . $uid);
        $serveRule = $this->ruleRepository->findByTable($table);
        $serveRule->initializeProcessor($systemRecordIdentifier, $identifier, $mapping);
        $serveRule->getDiff();

        $serveRule->updateDeployedData($serveRule->getDiff()->getDiffForDbRow(), $identifier);

        if (!$mapping) {
            $mapping = new \InstituteWeb\Serve\Domain\Model\Mapping\Mapping($identifier, $table . ':' . $uid);
            $mappingContainer = $this->ruleRepository->getDefaultMappingContainer();
            $mappingContainer->attach($mapping);
            $this->ruleRepository->getDefaultMappingContainer()->save();
        }
        \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($serveRule);
    }

    /**
     * @TODO Use extbase parameters
     * @TODO And add field list
     */
    public function importRecordAction()
    {
        $identifier = new SystemRecordIdentifier(GeneralUtility::_GET('identifier'));
        $serveRule = $this->ruleRepository->findByTable($identifier->getTable());
        $serveRule->initializeProcessor($identifier);
        $serveRule->getDiff();

        // TODO: Do TYPO3_DB update
    }
}
