<?php
namespace InstituteWeb\Serve\Controller;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Serve\Domain\ValueObject\SystemRecordIdentifier;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class RuleController
 *
 * @package InstituteWeb\Serve
 */
class RuleController extends AbstractBackendController
{
    /**
     * @var \InstituteWeb\Serve\Domain\Repository\RuleRepository
     * @inject
     */
    protected $ruleRepository;

    /**
     * @var \InstituteWeb\Serve\Domain\Repository\RecordRepository
     * @inject
     */
    protected $recordRepository;

    /**
     * @return void
     */
    public function showAction()
    {
        $systemRecordIdentifier = new SystemRecordIdentifier(GeneralUtility::_GET('identifier'));
        if (!$systemRecordIdentifier->getTable() || !$systemRecordIdentifier->getUid()) {
            $this->forward('index');
            return;
        }
        $this->view->assign('identifier', $systemRecordIdentifier);

        $serveRule = $this->ruleRepository->findByTable($systemRecordIdentifier->getTable());
        $serveRule->initializeProcessor($systemRecordIdentifier);
        $serveRule->getDiff();
        $this->view->assign('serveRule', $serveRule);

        // TODO: Wir müssen noch checken ob wir das serving aktivieren wollen


    }

    /**
     *
     * @return void
     */
    public function indexAction()
    {
//        $pageUid = GeneralUtility::_GET('id');
//        $this->view->assign('pageUid', $pageUid);
    }
}
