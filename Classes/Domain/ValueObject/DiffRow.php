<?php
namespace InstituteWeb\Serve\Domain\ValueObject;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Value object RowDiff
 *
 * @package InstituteWeb\Serve
 */
class DiffRow
{
    /**
     * @var array
     */
    private $systemData = [];

    /**
     * @var array
     */
    private $deployedData = [];

    /**
     * @var array
     */
    private $diffForDbRow = [];

    /**
     * @var array
     */
    private $diffForFileRow = [];

    /**
     * @var array Grouped diff, with db and file values combined, contains just different values
     */
    private $groupedDiff = [];

    /**
     * @var array Grouped diff, with all properties, also identical ones
     */
    private $groupedDiffFull = [];

    /**
     * True if deployed and system rows have differences. But false if one is not even existing.
     *
     * @var bool
     */
    private $hasDifferences = false;

    /**
     * True if this record is basically existing in deployment
     *
     * @var bool
     */
    private $_deployed = false;

    /**
     * True if this record is basically existing in system
     *
     * @var bool
     */
    private $_inSystem = false;

    /**
     * RowDiff constructor.
     *
     * @param array $dbRow
     * @param array $fileRow
     */
    public function __construct(array $dbRow, array $fileRow)
    {
        $this->systemData = $dbRow;
        $this->deployedData = $fileRow;
        $this->diffForDbRow = array_diff($this->systemData, $this->deployedData);
        $this->diffForFileRow = array_diff($this->deployedData, $this->systemData);
        $this->hasDifferences = !empty($this->diffForDbRow) && !empty($this->diffForFileRow);
        $this->_deployed = !empty($fileRow);
        $this->_inSystem = !empty($dbRow);
        $this->groupedDiff = $this->buildGroupedDiff($dbRow, $fileRow, true);
        $this->groupedDiffFull = $this->buildGroupedDiff($dbRow, $fileRow);
    }

    /**
     * Builds grouped Diff
     *
     * @param array $dbRow
     * @param array $fileRow
     * @param bool $stripIdenticalEntries
     * @return array
     */
    private function buildGroupedDiff(array $dbRow, array $fileRow, $stripIdenticalEntries = false)
    {
        $groupedDiff = [];

        if (empty($fileRow) && !empty($dbRow)) {
            foreach ($dbRow as $fieldKey => $fields) {
                $groupedDiff[$fieldKey]['db'] = $fields;
                $groupedDiff[$fieldKey]['file'] = null;
            }
            return $groupedDiff;
        }

        if (!empty($fileRow) && empty($dbRow)) {
            foreach ($dbRow as $fieldKey => $fields) {
                $groupedDiff[$fieldKey]['db'] = null;
                $groupedDiff[$fieldKey]['file'] = $fields;
            }
            return $groupedDiff;
        }

        $groupedDiff = array_merge_recursive($dbRow, $fileRow);
        foreach ($groupedDiff as $fieldKey => $fields) {
            if ($stripIdenticalEntries && $fields[0] === $fields[1]) {
                unset($groupedDiff[$fieldKey]);
                continue;
            }
            $groupedDiff[$fieldKey]['db'] = $fields[0];
            $groupedDiff[$fieldKey]['file'] = $fields[1];
        }
        return $groupedDiff;
    }

    /**
     * @return array
     */
    public function getSystemData()
    {
        return $this->systemData;
    }

    /**
     * @return array
     */
    public function getDeployedData()
    {
        return $this->deployedData;
    }

    /**
     * Shows which fields are different in db row
     *
     * @return string
     */
    public function getDiffForDbRow()
    {
        return $this->diffForDbRow;
    }

    /**
     * Shows which fields are different in file row
     *
     * @return string
     */
    public function getDiffForFileRow()
    {
        return $this->diffForFileRow;
    }

    /**
     * @return array
     */
    public function getGroupedDiff()
    {
        return $this->groupedDiff;
    }

    /**
     * @return array
     */
    public function getGroupedDiffFull()
    {
        return $this->groupedDiffFull;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $status = $this->isDifferent() ? ' has differences' : '';
        return '(RowDiff' . $status . ')';
    }

    /**
     * @return bool
     */
    public function isDifferent()
    {
        return $this->hasDifferences;
    }

    /**
     * @return bool
     */
    public function hasDifferences()
    {
        return $this->isDifferent();
    }

    /**
     * @return boolean
     */
    public function isIdentically()
    {
        return !$this->isDifferent();
    }

    /**
     * @return boolean
     */
    public function isDeployed()
    {
        return $this->_deployed;
    }

    /**
     * @return boolean
     */
    public function isInSystem()
    {
        return $this->_inSystem;
    }
}
