<?php
namespace InstituteWeb\Serve\Domain\ValueObject;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Value object SystemRecordIdentifier
 *
 * @package InstituteWeb\Serve
 */
class SystemRecordIdentifier
{
    /**
     * @var string
     */
    private $value = '';

    /**
     * @var string
     */
    private $table = '';

    /**
     * @var int
     */
    private $uid = 0;

    /**
     * SystemRecordIdentifier constructor.
     *
     * @param string $value A system record identifier like e.g. "tt_content:42"
     */
    public function __construct($value)
    {
        if (!is_string($value) || !preg_match('/^.*\:\d*$/', $value)) {
            throw new \InvalidArgumentException(
                'Given value is no valid system record identifier like e.g. "tt_content:42". Given: ' .
                var_export($value, true)
            );
        }
        $this->value = $value;
        list($this->table, $this->uid) = explode(':', trim($value));
        $this->uid = (int) $this->uid;
    }

    /**
     * @return string
     */
    public function get()
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @return int
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->get();
    }
}
