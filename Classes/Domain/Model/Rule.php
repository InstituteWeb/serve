<?php
namespace InstituteWeb\Serve\Domain\Model;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Serve\Domain\Model\Processor\AbstractProcessor;

/**
 * Class Rule
 *
 * @package InstituteWeb\Serve
 */
class Rule
{
    /**
     * @var string
     */
    private $table;

    /**
     * @var AbstractProcessor
     */
    protected $processor;

    /**
     * @var string
     */
    protected $identifier;

    /**
     * Rule constructor.
     *
     * @param $table
     */
    public function __construct($table)
    {
        $this->table = $table;
    }

    /**
     * @param string $systemIdentifier
     * @param string $identifier
     * @param Mapping\Mapping|null $mapping
     */
    public function initializeProcessor($systemIdentifier, $identifier = null, \InstituteWeb\Serve\Domain\Model\Mapping\Mapping $mapping = null)
    {
        $this->identifier = $identifier ?: $systemIdentifier;
        $this->processor->initialize($systemIdentifier, $this->identifier, $mapping);
    }

    /**
     * @return AbstractProcessor
     */
    public function getProcessor()
    {
        return $this->processor;
    }

    /**
     * @return \InstituteWeb\Serve\Domain\ValueObject\DiffRow
     */
    public function getDiff()
    {
        return $this->processor->diff();
    }

    /**
     * @param AbstractProcessor $processor
     */
    public function setProcessor($processor)
    {
        $this->processor = $processor;
    }

    /**
     * Also clones child elements
     */
    public function __clone()
    {
        $this->processor = clone $this->processor;
    }

    /**
     * @param mixed $data
     * @param $identifier
     */
    public function updateDeployedData($data, $identifier)
    {
        $this->processor->updateDeployedData($data, $identifier ?: $this->identifier);
    }

    /**
     * @param mixed $data
     */
    public function updateSystemData($data)
    {
        $this->processor->updateSystemData($data);
    }

    /**
     * @param string $identifier
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
        $this->processor->setIdentifier($identifier);
    }
}
