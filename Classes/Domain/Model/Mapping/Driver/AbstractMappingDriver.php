<?php
namespace InstituteWeb\Serve\Domain\Model\Mapping\Driver;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Serve\Domain\Model\Mapping\Mapping;

/**
 * Abstract class AbstractMappingDriver
 *
 * @package InstituteWeb\Serve
 */
abstract class AbstractMappingDriver
{

    /**
     * @return Mapping[]
     */
    public function get() {
    }

    /**
     * @param Mapping[] $mappings
     */
    public function save($mappings)
    {
    }
}
