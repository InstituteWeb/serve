<?php
namespace InstituteWeb\Serve\Domain\Model\Mapping\Driver;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Serve\Domain\Model\Mapping\Mapping;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class SysRegistryMappingDriver
 *
 * @package InstituteWeb\Serve
 */
class SysRegistryMappingDriver extends AbstractMappingDriver
{
    /**
     * Get data from sys_registry and converts it to an array of Mappings
     *
     * @return Mapping[]
     */
    public function get()
    {
        /** @var \TYPO3\CMS\Core\Registry $registry */
        $registry = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Registry::class);
        $mappings = [];
        foreach ($registry->get('serve', 'mappings', []) as $identifier => $systemIdentifier) {
            $mappings[] = new Mapping($identifier, $systemIdentifier);
        };
        return $mappings;
    }

    /**
     * Convert array of Mappings to sys_registry (and stores it)
     *
     * @param Mapping[] $mappings
     */
    public function save($mappings)
    {
        if (!count($mappings)) {
            throw new \InvalidArgumentException('SysRegistryMappingDriver expects an array or iterable as data to store.');
        }

        $data = [];
        foreach ($mappings as $mapping) {
            $data[$mapping->getIdentifier()] = $mapping->getSystemTable() . ':' . $mapping->getSystemUid();
        }

        /** @var \TYPO3\CMS\Core\Registry $registry */
        $registry = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Registry::class);
        $registry->set('serve', 'mappings', $data);
    }
}
