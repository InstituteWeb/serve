<?php
namespace InstituteWeb\Serve\Domain\Model\Mapping\Driver;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Class FileMappingDriver
 *
 * @package InstituteWeb\Serve
 */
class FileMappingDriver extends AbstractMappingDriver
{

//    $recordRepository

    public function __construct()
    {

    }

    /**
     * @return \InstituteWeb\Serve\Domain\Model\Mapping\Mapping[]
     */
    public function get()
    {
        // TODO: Hole daten aus der datenbank
    }
}
