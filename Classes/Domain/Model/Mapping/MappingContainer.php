<?php
namespace InstituteWeb\Serve\Domain\Model\Mapping;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Class MappingContainer
 *
 * @package InstituteWeb\Serve
 */
class MappingContainer extends \TYPO3\CMS\Extbase\Persistence\ObjectStorage
{
    /**
     * @var bool
     */
    protected $_loaded = false;

    /**
     * @var Mapping[]
     */
    protected $storage = [];

    /**
     * @var Driver\AbstractMappingDriver
     */
    protected $driver;


    /**
     * Loads the mapping with assigned driver and initializes this mapping container
     *
     * @return MappingContainer The Mapping Container itself
     */
    public function load()
    {
        $this->removeAll($this);
        foreach ($this->driver->get() as $entry) {
            $this->attach($entry);
        }
        $this->_loaded = true;
        return $this;
    }

    /**
     * Checks if given identifier is in this mapping container located.
     *
     * @param string $identifier
     * @return bool
     */
    public function has($identifier)
    {
        return array_key_exists($identifier, $this->storage);
    }

    /**
     * @param string $identifier
     * @return Mapping|null
     */
    public function get($identifier)
    {
        if (!array_key_exists((string) $identifier, $this->storage)) {
            return null;
        }
        return $this->storage[(string)$identifier]['obj'];
    }

    /**
     * @param Driver\AbstractMappingDriver $driver
     * @return void
     * @throws \RuntimeException
     */
    public function setDriver(Driver\AbstractMappingDriver $driver)
    {
        if ($this->driver) {
            throw new \RuntimeException('Driver already set in MappingContainer.');
        }
        $this->driver = $driver;
    }

    /**
     * @param Mapping $object
     * @param mixed $information The data to associate with the object
     * @throws \InvalidArgumentException
     */
    public function attach($object, $information = null)
    {
        if (!$object instanceof Mapping) {
            throw new \InvalidArgumentException('Mapping container just accept Mapping objects.');
        }
        parent::attach($object, $information);
    }

    /**
     * Saves the mapping with assigned driver
     *
     * @return void
     */
    public function save()
    {
        $this->driver->save($this);
    }

    // Use $mapping->getIdentifier() as array key. Overwriting ObjectStorage from here.

    /**
     * Associates data to an object in the storage. offsetSet() is an alias of attach().
     *
     * @param Mapping $object The object to add.
     * @param mixed $information The data to associate with the object.
     * @return void
     */
    public function offsetSet($object, $information)
    {
        $this->isModified = true;
        $this->storage[$object->getIdentifier()] = ['obj' => $object, 'inf' => $information];

        $this->positionCounter++;
        $this->addedObjectsPositions[$object->getIdentifier()] = $this->positionCounter;
    }

    /**
     * Checks whether an object exists in the storage.
     *
     * @param Mapping $object The object to look for.
     * @return bool
     */
    public function offsetExists($object)
    {
        return is_object($object) && isset($this->storage[$object->getIdentifier()]);
    }

    /**
     * Removes an object from the storage. offsetUnset() is an alias of detach().
     *
     * @param Mapping $object The object to remove.
     * @return void
     */
    public function offsetUnset($object)
    {
        $this->isModified = true;
        unset($this->storage[spl_object_hash($object)]);

        if (empty($this->storage)) {
            $this->positionCounter = 0;
        }

        $this->removedObjectsPositions[$object->getIdentifier()] = $this->addedObjectsPositions[$object->getIdentifier()];
        unset($this->addedObjectsPositions[$object->getIdentifier()]);
    }

    /**
     * Returns the data associated with an object.
     *
     * @param Mapping $object The object to look for.
     * @return mixed The data associated with an object in the storage.
     */
    public function offsetGet($object)
    {
        return $this->storage[$object->getIdentifier()]['inf'];
    }

    /**
     * @param string $systemIdentifier
     * @return string|null Identifier
     */
    public function getIdentifierBySystemIdentifier($systemIdentifier)
    {
        foreach ($this->storage as $mapping)
        {
            /** @var Mapping $mapping */
            $mapping = $mapping['obj'];
            if ($systemIdentifier == $mapping->getSystemTable() . ':' . $mapping->getSystemUid()) {
                return $mapping->getIdentifier();
            }
        }
        return null;
    }
}
