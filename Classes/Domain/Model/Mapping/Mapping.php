<?php
namespace InstituteWeb\Serve\Domain\Model\Mapping;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Serve\Domain\Model\Source\AbstractSource;

/**
 * Class Mapping
 *
 * @package InstituteWeb\Serve
 */
class Mapping
{
    /**
     * @var string Identifier like "template.main"
     */
    private $identifier;

    /**
     * @var string Record table
     */
    private $systemTable;

    /**
     * @var string|int Record uid
     */
    private $systemUid;

    /**
     * @var AbstractSource The associated system data
     */
    private $systemData;

    /**
     * Mapping constructor
     *
     * @param string $identifier
     * @param string $systemIdentifier
     */
    public function __construct($identifier, $systemIdentifier)
    {
        $this->identifier = $identifier;

        $systemIdentifierParts = explode(':', $systemIdentifier);
        $this->systemTable = $systemIdentifierParts[0];
        $this->systemUid = (int) $systemIdentifierParts[1];
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }

    /**
     * Get system's unique identifier
     *
     * @return string|int
     */
    public function getSystemUid()
    {
        return $this->systemUid ?: $this->systemData->getData()['uid'] ?: $this->systemData->getData()['id'] ?: '';
    }

    /**
     * @return AbstractSource
     */
    public function getSystemData()
    {
        return $this->systemData;
    }

    /**
     * @param AbstractSource $systemData
     * @return void
     */
    public function setSystemData(AbstractSource $systemData)
    {
        $this->systemData = $systemData;
    }

    /**
     * @return string
     */
    public function getSystemTable()
    {
        return $this->systemTable;
    }

    /**
     * @return string
     */
    public function getSystemIdentifier()
    {
        return $this->systemTable . ':' . $this->systemUid;
    }
}
