<?php
namespace InstituteWeb\Serve\Domain\Model\Processor;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Serve\Domain\Model\Mapping;
use InstituteWeb\Serve\Domain\Model\Source\AbstractSource;
use InstituteWeb\Serve\Domain\ValueObject\SystemRecordIdentifier;
use InstituteWeb\Serve\Utility\InstanceUtility;
use InstituteWeb\Serve\Utility\PageTS;

/**
 * Abstract class AbstractProcessor
 *
 * @package InstituteWeb\Serve
 */
abstract class AbstractProcessor
{
    /**
     * True when the processor has been instantiated with data (DB row)
     *
     * @var bool
     */
    protected $_initialized = false;

    /**
     * True when processor has diffed the DB row with given one
     *
     * @var bool
     */
    protected $_diffed = false;

    /**
     * @var \InstituteWeb\Serve\Domain\ValueObject\DiffRow
     */
    protected $diff;

    /**
     * @var bool
     */
    protected $hasMapping = false;

    /**
     * @var bool
     */
    protected $isInSync = false;

    /**
     * @var array
     */
    protected $options = [
        'allow' => '',
        'exclude' => '',
    ];

    /**
     * @var string
     */
    protected $tableName = '';

    /**
     * @var string
     */
    protected $identifier;

//    /**
//     * The original row
//     *
//     * @var array
//     */
//    protected $dbRow = [];
//
//    /**
//     * The filtered row
//     *
//     * @var array
//     */
//    protected $filteredDbRow = [];
//
//    /**
//     * The differences of both rows
//     *
//     * @var \InstituteWeb\Serve\Domain\ValueObject\DiffRow
//     */
//    protected $diffRow;

    /**
     * @var AbstractSource
     */
    protected $systemData;

    /**
     * @var AbstractSource
     */
    protected $deployedData;

    /**
     * @var Mapping\MappingContainer
     */
    protected $mappingContainer;

    /**
     * FileProcessor constructor.
     *
     * @param array $configuration
     * @param string $tableName
     * @param Mapping\MappingContainer $mapper
     */
    public function __construct(array $configuration, $tableName, Mapping\MappingContainer $mapper)
    {
        // Setup basic options, with defined default values (so options contain all possible/registred values)
        $propertyNameRegisterOptions = 'registerOptions';
        if (property_exists($this, $propertyNameRegisterOptions) && !empty($this->$propertyNameRegisterOptions)) {
            $this->options = array_merge($this->options, $this->$propertyNameRegisterOptions);
        }

        unset($configuration['_typoScriptNodeValue']);
        $this->options = array_merge($this->options, $configuration);

        $this->tableName = $tableName;
        $this->mappingContainer = $mapper;
    }

    /**
     * Initializes this processor with systemData
     *
     * @param string $systemIdentifier
     * @param string $identifier
     * @param Mapping\Mapping $mapping
     */
    public function initialize($systemIdentifier, $identifier, Mapping\Mapping $mapping = null)
    {
        if ($this->_initialized) {
            return;
        }
        $this->identifier = (string) $identifier;
        $this->systemData = new \InstituteWeb\Serve\Domain\Model\Source\DatabaseRow($mapping ?: $systemIdentifier);
        $this->mappingContainer->load();
        $this->_initialized = true;
    }

    /**
     * @return \InstituteWeb\Serve\Domain\ValueObject\DiffRow
     */
    public function diff()
    {
        $deployedData = $this->deployedData ? $this->deployedData->getData() : [];
        $diffRow = new \InstituteWeb\Serve\Domain\ValueObject\DiffRow(
            $this->filterRowColumns($this->systemData->getData()),
            $this->filterRowColumns($deployedData)
        );
        $this->diff = $diffRow;
        $this->_diffed = true;
        return $diffRow;
    }

    /**
     * @return \InstituteWeb\Serve\Domain\ValueObject\DiffRow
     */
    public function getDiff()
    {
        return $this->diff();
    }

    /**
     * Updates system data
     *
     * @param mixed $data if null $this->deployedData is taken
     * @param string $identifier
     * @return bool
     */
    public function updateSystemData($data = null, $identifier = null)
    {
        return $this->systemData->updateData($data ?: $this->deployedData, $identifier ?: $this->identifier);
    }

    /**
     * Updates deployed data
     *
     * @param mixed $data if null $this->systemData is taken
     * @param string $identifier
     * @return bool
     */
    public function updateDeployedData($data = null, $identifier = null)
    {
        return $this->deployedData->updateData($data ?: $this->systemData, $identifier ?: $this->identifier);
    }

    /**
     * Removes/deletes source's content
     *
     * @return bool
     */
    public function purge()
    {
        return $this->deployedData->removeData();
    }

    /**
     * @return AbstractSource
     */
    public function getDeployedData()
    {
        return $this->deployedData;
    }

    /**
     * Protected setter of deployedData
     * @param AbstractSource $deployedData
     * @return void
     */
    protected function setDeployedData(AbstractSource $deployedData)
    {
        $this->deployedData = $deployedData;
    }

    /**
     * @return AbstractSource
     */
    public function getSystemData()
    {
        return $this->systemData;
    }

    /**
     * Protected setter of systemData
     *
     * @param AbstractSource $systemData
     * @return void
     */
    protected function setSystemData(AbstractSource $systemData)
    {
        $this->systemData = $systemData;
    }

    /**
     * @param array $row
     * @return array the filtered row
     */
    protected function filterRowColumns(array $row)
    {
        $filteredRow = [];
        $exclude = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', $this->options['exclude'], true);
        $allow = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', $this->options['allow'], true);
        foreach ($row as $attribute => $value) {
            if (in_array($attribute, $allow)) {
                $filteredRow[$attribute] = $value;
                continue;
            }
            if (!in_array($attribute, $exclude) && !$allow) {
                $filteredRow[$attribute] = $value;
            }
        }
        return $filteredRow;
    }

    /**
     * Also clones its storage
     *
     * @return void
     */
    public function __clone()
    {
        $this->systemData = is_object($this->systemData) ? clone $this->systemData : $this->systemData;
        $this->deployedData = is_object($this->deployedData) ? clone $this->deployedData : $this->deployedData;
    }

    /**
     * @param string $identifier
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }
}
