<?php
namespace InstituteWeb\Serve\Domain\Model\Container;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Serve\Domain\Model\Source\File\AbstractFile;

/**
 * Class FileContainer
 *
 * @package InstituteWeb\Serve
 */
class FileContainer extends \TYPO3\CMS\Extbase\Persistence\ObjectStorage
{
    /**
     * @var AbstractFile[]
     */
    protected $storage = [];

    /**
     * @param AbstractFile $object
     * @param mixed $information The data to associate with the object
     * @throws \InvalidArgumentException
     */
    public function attach($object, $information = null)
    {
        if (!$object instanceof AbstractFile) {
            throw new \InvalidArgumentException('File container just accept objects extended from AbstractFile.');
        }
        parent::attach($object, $information);
    }
}
