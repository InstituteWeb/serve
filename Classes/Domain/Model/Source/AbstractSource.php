<?php
namespace InstituteWeb\Serve\Domain\Model\Source;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Class AbstractSource
 *
 * @package InstituteWeb\Serve
 */
abstract class AbstractSource
{
    /**
     * @var string
     */
    protected $identifier;

    /**
     * @var bool
     */
    protected $isExisting = false;

    /**
     * @var array
     */
    protected $data = [];

    /**
     * Returns content row
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Update source's content
     *
     * @param mixed $data
     * @param string $identifier
     * @return bool
     */
    public function updateData($data, $identifier)
    {
        $this->data = $data;
        $this->identifier = $identifier;
        return true;
    }

    /**
     * Removes source's content
     *
     * @return bool
     */
    public function removeData()
    {
        $this->isExisting = false;
        $this->data = [];
        return true;
    }
}
