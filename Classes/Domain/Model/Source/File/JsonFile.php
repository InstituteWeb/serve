<?php
namespace InstituteWeb\Serve\Domain\Model\Source\File;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Class JsonFile
 *
 * @package InstituteWeb\Serve
 */
class JsonFile extends AbstractFile
{
    /**
     * Loads data from file and builds row
     *
     * @return array
     */
    protected function convertFileDataToRow()
    {
        $content = $this->loadFileContent();
        if (!$content) {
            return [];
        }
        return json_decode($content, true);
    }

    /**
     * Returns data to write to file, based on current row
     *
     * @param array $row
     * @return string
     */
    protected function convertRowToFileData(array $row)
    {
        return json_encode($row ?: $this->data, JSON_PRETTY_PRINT);
    }
}
