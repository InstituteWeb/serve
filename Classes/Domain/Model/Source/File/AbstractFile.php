<?php
namespace InstituteWeb\Serve\Domain\Model\Source\File;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Class AbstractFile
 *
 * @package InstituteWeb\Serve
 */
abstract class AbstractFile extends \InstituteWeb\Serve\Domain\Model\Source\AbstractSource
{
    /**
     * @var string
     */
    protected $originalPath = '';

    /**
     * @var string
     */
    protected $directory;

    /**
     * @var string
     */
    protected $fileName;

    /**
     * @var string
     */
    protected $path = '';

    /**
     * @var string
     */
    protected $identifier;

    /**
     * @var int unix timestamp
     */
    protected $modificationTime = 0;

    /**
     * File constructor.
     *
     * @param string $path
     */
    public function __construct($path)
    {
        $this->updatePath($path);

        if ($this->isExisting && method_exists($this, 'convertFileDataToRow')) {
            $this->modificationTime = filemtime($this->path);
            $this->data = $this->convertFileDataToRow();
        }
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function updatePath($path)
    {
        $this->originalPath = $path;
        $parts = explode('/', trim($path));
        $this->fileName = (string) array_pop($parts);
        $this->directory = implode('/', $parts);

        $this->path = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($path);
        $this->isExisting = file_exists($this->path);
    }

    /**
     * @return null|string
     */
    protected function loadFileContent()
    {
        if (file_exists($this->path)) {
            return file_get_contents($this->path);
        }
        return null;
    }

    /**
     * @param array $data
     * @param string $identifier
     * @return bool
     * @throws \Exception
     */
    public function updateData($data, $identifier)
    {
        if (!is_array($data)) {
            throw new \InvalidArgumentException('Database row requires an array with updated fields (key=fieldname).');
        }

        \InstituteWeb\Serve\Domain\Model\Source\AbstractSource::updateData($data, $identifier);
        if (!method_exists($this, 'convertRowToFileData')) {
            // TODO: Write interface for that (for whole Processor)
            throw new \Exception('The processor "' . get_class($this) . '" must implement a convertRowToFileData method.');
        }
        $content = $this->convertRowToFileData($data);
        //TODO: Check if folder exists
        return file_put_contents($this->path, $content);
    }

    /**
     * Removes the file completely
     *
     * @return bool
     */
    public function removeData()
    {
        \InstituteWeb\Serve\Domain\Model\Source\AbstractSource::removeData();
        return unlink($this->path);
    }
}
