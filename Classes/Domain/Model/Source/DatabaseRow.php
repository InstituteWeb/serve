<?php
namespace InstituteWeb\Serve\Domain\Model\Source;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Serve\Domain\Model\Mapping\Mapping;
use InstituteWeb\Serve\Domain\Repository\RecordRepository;
use InstituteWeb\Serve\Domain\ValueObject\SystemRecordIdentifier;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Final class DatabaseRow
 *
 * @package InstituteWeb\Serve
 */
final class DatabaseRow extends AbstractSource
{
    /**
     * @var int
     */
    private $uid = 0;

    /**
     * @var string
     */
    private $table;

    /**
     * @var RecordRepository
     */
    private $_recordRepository;

    /**
     * DatabaseRow constructor
     *
     * @param Mapping|SystemRecordIdentifier $identifier
     */
    public function __construct($identifier)
    {
        $this->_recordRepository = GeneralUtility::makeInstance(RecordRepository::class);

        if ($identifier instanceof Mapping) {
            $this->data = $this->_recordRepository->getByMapping($identifier);
            $this->table = $identifier->getTable();
            $this->uid = $identifier->getSystemUid();
        } else if ($identifier instanceof SystemRecordIdentifier) {
            $this->data = $this->_recordRepository->getBySystemRecordIdentifier($identifier);
            $this->table = $identifier->getTable();
            $this->uid = $this->data['uid'] ?: 0;
        }
    }

    /**
     * Update source's content
     *
     * @param array $data
     * @param string $identifier
     * @return bool
     */
    public function updateData($data, $identifier)
    {
        if (!is_array($data)) {
            throw new \InvalidArgumentException('Database row requires an array with updated fields (key=fieldname).');
        }
        $this->data = $data;
        // TODO: Transform the data
        $this->_recordRepository->update($this->table, $this->uid ?: $data['uid'], $data);
        return true;
    }

    /**
     * Removes source's content
     *
     * @return bool
     */
    public function removeData()
    {
        $this->isExisting = false;
        $this->data = [];
        $this->_recordRepository->remove($this->table, $this->uid);
        return true;
    }

    /**
     * @return int
     */
    public function getUid()
    {
        return $this->uid;
    }
}
