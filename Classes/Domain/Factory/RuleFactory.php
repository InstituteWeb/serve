<?php
namespace InstituteWeb\Serve\Domain\Factory;

    /*  | This extension is part of the TYPO3 project. The TYPO3 project is
     *  | free software and is licensed under GNU General Public License.
     *  |
     *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
     */
use InstituteWeb\Serve\Domain\ValueObject\SystemRecordIdentifier;

/**
 * Class ServeRuleFactory
 *
 * @package InstituteWeb\Serve
 */
class RuleFactory implements \TYPO3\CMS\Core\SingletonInterface
{
    /**
     * @var \InstituteWeb\Serve\Domain\Repository\RuleRepository
     * @inject
     */
    protected $ruleRepository;

    /**
     * @var \InstituteWeb\Serve\Domain\Repository\RecordRepository
     * @inject
     */
    protected $recordRepository;

    /**
     * @param SystemRecordIdentifier $identifier
     * @param bool $loadRowFromDb
     * @param bool $performDiff
     * @return \InstituteWeb\Serve\Domain\Model\Rule|null
     */
    public function create(SystemRecordIdentifier $identifier, $loadRowFromDb = true, $performDiff = true)
    {
        $rule = $this->ruleRepository->findByTable($identifier->getTable());
        if ($rule instanceof \InstituteWeb\Serve\Domain\Model\Rule && $loadRowFromDb) {
            $rule = clone $rule;
            $rule->initializeProcessor(
                $identifier,
                $this->ruleRepository->getDefaultMappingContainer()->get($identifier)
            );
            if ($performDiff) {
                $rule->getDiff();
            }
        }
        return $rule;
    }
}
