<?php
namespace InstituteWeb\Serve\Domain\Repository;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Serve\Domain\Model\Mapping;
use InstituteWeb\Serve\Utility\InstanceUtility;

/**
 * Class RuleRepository
 *
 * @package InstituteWeb\Serve
 */
class RuleRepository implements \TYPO3\CMS\Core\SingletonInterface
{
    /**
     * @var array PageTS configuration of tx_serve
     */
    protected $configuration = [];

    /**
     * @var \InstituteWeb\Serve\Domain\Model\Rule[]
     */
    protected $rules = [];

    /**
     * @var Mapping\MappingContainer
     */
    protected $defaultMappingContainer;

    /**
     * ServeRuleRepository constructor.
     */
    public function __construct()
    {
        // TODO: Move this out of the constructor
        $this->configuration = \InstituteWeb\Serve\Utility\PageTS::get('tx_serve', []);

        $defaultMapperDriver = InstanceUtility::getInstanceByAbstract(
            Mapping\Driver\AbstractMappingDriver::class,
            ($this->configuration['mapper']['default']['driver'] ?: 'Database') . 'MappingDriver'
        );
        $defaultMapper = new Mapping\MappingContainer();
        $defaultMapper->setDriver($defaultMapperDriver);

        // The wording is "mapper" in PageTS configuration and "mappingContainer" in code
        $this->defaultMappingContainer = $mappingContainer = $defaultMapper;

        if ($this->configuration['rules']) {
            foreach ($this->configuration['rules'] as $tableName => $rule) {
                if ($rule['mapper'] && $rule['mapper'] !== 'default') {
                    // TODO: Get other registred mapper for each rule and overwrite $mappingContainer
                    // $mapper = ...
                }

                // Load mappings from configured driver. $mapper itself is iterable and contains mappings.
                $mappingContainer->load();

                if ($rule['enabled']) {
                    if (!is_array($this->rules[$tableName])) {
                        $this->rules[$tableName] = [];
                    }
                    $serveRule = new \InstituteWeb\Serve\Domain\Model\Rule($tableName);

                    $processor = InstanceUtility::getInstanceByAbstract(
                        \InstituteWeb\Serve\Domain\Model\Processor\AbstractProcessor::class,
                        $rule['processor']['_typoScriptNodeValue'],
                        $rule['processor'],
                        $tableName,
                        $mappingContainer
                    );
                    $serveRule->setProcessor($processor);
                    $this->rules[$tableName] = $serveRule;
                }
            }
        }
    }

    /**
     * @param string $tableName
     * @return \InstituteWeb\Serve\Domain\Model\Rule|null
     */
    public function findByTable($tableName)
    {
        if (array_key_exists($tableName, $this->rules)) {
            $clone = $this->rules[$tableName];
            return $clone;
        }
        return null;
    }

    /**
     * @return Mapping\MappingContainer
     */
    public function getDefaultMappingContainer()
    {
        return $this->defaultMappingContainer;
    }
}
