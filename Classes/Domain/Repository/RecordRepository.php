<?php
namespace InstituteWeb\Serve\Domain\Repository;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Serve\Domain\Model\Mapping\Mapping;
use InstituteWeb\Serve\Domain\ValueObject\SystemRecordIdentifier;

/**
 * Class RecordRepository
 *
 * @package InstituteWeb\Serve
 */
class RecordRepository implements \TYPO3\CMS\Core\SingletonInterface
{
    /**
     * @var \TYPO3\CMS\Core\Database\ConnectionPool
     */
    private $connectionPool;

    /**
     * RecordRepository constructor.
     */
    public function __construct()
    {
        /** @var \TYPO3\CMS\Core\Database\Query\QueryBuilder $queryBuilder */
        $this->connectionPool = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
            \TYPO3\CMS\Core\Database\ConnectionPool::class
        );
    }

    /**
     * Get record from database as assoc array. Returns false on any error.
     *
     * @param string $table
     * @param int $uid
     * @return array|bool
     */
    public function get($table, $uid)
    {
        $queryBuilder = $this->connectionPool->getQueryBuilderForTable($table);
        return $queryBuilder
            ->select('*')
            ->from($table)
            ->where($queryBuilder->expr()->eq('uid', $uid))
            ->execute()->fetch(0);
    }

    /**
     * Same as ->get() but accepts SystemRecordIdentifier
     *
     * @param SystemRecordIdentifier $identifier
     * @return array|bool
     */
    public function getBySystemRecordIdentifier(SystemRecordIdentifier $identifier)
    {
        return $this->get($identifier->getTable(), $identifier->getUid());
    }

    /**
     * Same as ->get() but accepts Mapping
     *
     * @param Mapping $mapping
     * @return array|bool
     */
    public function getByMapping(Mapping $mapping)
    {
        return $this->get($mapping->getSystemTable(), $mapping->getSystemUid());
    }

    /**
     * @param $table
     * @param $uid
     * @param array $data
     * @TODO
     */
    public function update($table, $uid, array $data)
    {
    }

    /**
     * @param $table
     * @param $uid
     * @TODO
     */
    public function remove($table, $uid)
    {
    }
}
