<?php
namespace InstituteWeb\Serve\Hooks;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Serve\Domain\Repository\RuleRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Export Button Hook for list view
 *
 * @package InstituteWeb\Serve
 */
class DumpButton implements \TYPO3\CMS\Recordlist\RecordList\RecordListHookInterface
{
    /**
     * @var \TYPO3\CMS\Core\Page\PageRenderer
     */
    protected static $pageRenderer;

    /**
     * Modifies Web>List clip icons (copy, cut, paste, etc.) of a displayed row
     *
     * @param string $table The current database table
     * @param array $row The current record row
     * @param array $cells The default clip-icons to get modified
     * @param \TYPO3\CMS\Recordlist\RecordList\DatabaseRecordList $parentObject Instance of calling object
     * @return array The modified clip-icons
     */
    public function makeClip($table, $row, $cells, &$parentObject)
    {
        if ($this->getBackendUserAuthentication()->isAdmin()) {
            /** @var \TYPO3\CMS\Core\Imaging\IconFactory $iconFactory */
            $iconFactory = GeneralUtility::makeInstance('TYPO3\CMS\Core\Imaging\IconFactory');
            $icon = (string) $iconFactory->getIcon('apps-pagetree-folder-contains-shop', \TYPO3\CMS\Core\Imaging\Icon::SIZE_SMALL);

            /** @var \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager */
            $objectManager = GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class);

            $identifier = new \InstituteWeb\Serve\Domain\ValueObject\SystemRecordIdentifier($table . ':' . $row['uid']);

            /** @var \InstituteWeb\Serve\Domain\Factory\RuleFactory $ruleFactory */
            $ruleFactory = $objectManager->get(\InstituteWeb\Serve\Domain\Factory\RuleFactory::class);
            $rule = $ruleFactory->create($identifier);
            if (!$rule) {
                return $cells;
            }

            \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($rule, 'Rule');
            /** @var \TYPO3\CMS\Backend\Routing\UriBuilder $uriBuilder */
            $uriBuilder = $objectManager->get(\TYPO3\CMS\Backend\Routing\UriBuilder::class);

            // TODO: What to do with "tx_serve_system_serveservemodule"?
            $a = $uriBuilder->buildUriFromModule('system_ServeServemodule', [
                'identifier' => $table . ':' . $row['uid'],
                'tx_serve_system_serveservemodule[action]' => 'show',
                'tx_serve_system_serveservemodule[controller]' => 'Rule'
            ]);

            $diff = $rule->getDiff();
            if ($diff->isDeployed()) {
                $style = 'style="background-color: darkgreen;"';
            } else {
//                $style = 'style="background-color: #999;"';
            }

            if ($diff->hasDifferences()) {
                $style = 'style="background-color: darkred;"';
            }

//            /** @var RuleRepository $serveRuleRepository */
//            $serveRuleRepository = GeneralUtility::makeInstance(RuleRepository::class);
//            $serveRule = $serveRuleRepository->findByTable($table);
//            if (!$serveRule) {
//                return $cells;
//            }
            // TODO: Get status and display in link (as bg color, e.g.)

            $cells['serveModuleLink'] = '<a ' . $style . ' href="' . $a . '" class="serve-module-link btn btn-default" data-table="' .
                $table . '" data-uid="' . $row['uid'] . '">' . $icon . '</a>';
        }
        return $cells;
    }

    /**
     * Returns instance of PageRenderer to be able to influence styles and js in backend
     *
     * @return \TYPO3\CMS\Core\Page\PageRenderer
     */
    protected function getPageRenderer()
    {
        if (self::$pageRenderer instanceof \TYPO3\CMS\Core\Page\PageRenderer) {
            return self::$pageRenderer;
        }

        /** @var \TYPO3\CMS\Backend\Template\DocumentTemplate $mediumDocumentTemplate */
        $mediumDocumentTemplate = GeneralUtility::makeInstance('TYPO3\CMS\Backend\Template\DocumentTemplate');
        /** @var \TYPO3\CMS\Core\Page\PageRenderer $pr */
        self::$pageRenderer = $mediumDocumentTemplate->getPageRenderer();
        return self::$pageRenderer;
    }

    /**
     * Returns current backend user
     *
     * @return \TYPO3\CMS\Core\Authentication\BackendUserAuthentication
     */
    protected function getBackendUserAuthentication()
    {
        return $GLOBALS['BE_USER'];
    }

    /**
     * Modifies Web>List control icons of a displayed row
     * Unused but required because of interface.
     *
     * @param string $table The current database table
     * @param array $row The current record row
     * @param array $cells The default control-icons to get modified
     * @param object $parentObject Instance of calling object
     * @return array The modified control-icons
     */
    public function makeControl($table, $row, $cells, &$parentObject)
    {
        return $cells;
    }

    /**
     * Modifies Web>List header row columns/cells
     * Unused but required because of interface.
     *
     * @param string $table The current database table
     * @param array $currentIdList Array of the currently displayed uids of the table
     * @param array $headerColumns An array of rendered cells/columns
     * @param object $parentObject Instance of calling (parent) object
     * @return array Array of modified cells/columns
     */
    public function renderListHeader($table, $currentIdList, $headerColumns, &$parentObject)
    {
        return $headerColumns;
    }

    /**
     * Modifies Web>List header row clipboard/action icons
     * Unused but required because of interface.
     *
     * @param string $table The current database table
     * @param array $currentIdList Array of the currently displayed uids of the table
     * @param array $cells An array of the current clipboard/action icons
     * @param object $parentObject Instance of calling (parent) object
     * @return array Array of modified clipboard/action icons
     */
    public function renderListHeaderActions($table, $currentIdList, $cells, &$parentObject)
    {
        return $cells;
    }
}
