<?php
namespace InstituteWeb\Serve\Utility;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * InstanceUtility
 *
 * @package InstituteWeb\Serve
 */
class InstanceUtility
{
    /**
     * Creates new instance by given abstract classname. The second parameter is the classname.
     *
     * Example:
     * > "\Vendor\Package\Models\AbstractModel"
     * > "CustomerModel"
     * >> Will create an instance of \Vendor\Package\Models\CustomerModel
     *
     * Further three parameters can get passed to the constructor of new instance.
     *
     * @param string $abstractClassName Class name of abstract class
     * @param string $replaceLastPartWith Replace the class name part with this string
     * @param mixed $firstParameter for constructor of new instance
     * @param mixed $secondParameter for constructor of new instance
     * @param mixed $thirdParameter for constructor of new instance
     * @return object New Instance
     */
    public static function getInstanceByAbstract(
        $abstractClassName,
        $replaceLastPartWith,
        $firstParameter = null,
        $secondParameter = null,
        $thirdParameter = null
    ) {
        $classNameParts = explode('\\', $abstractClassName);
        array_pop($classNameParts);
        $namespace = implode('\\', $classNameParts) . '\\';

        $className = $namespace . $replaceLastPartWith;
        if (!class_exists($className)) {
            throw new \InvalidArgumentException('No class "' . $className . '" found.');
        }

        return new $className($firstParameter, $secondParameter, $thirdParameter);
    }
}
